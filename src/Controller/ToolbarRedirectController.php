<?php

namespace Drupal\ts_dx\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller redirecting to an edition form page.
 *
 * For example, to redirect to the last homepage node edition form:
 * custom_admin_menu.homepage:
 *   title: Home
 *   parent: custom_admin_menu
 *   route_name: ts_dx.node_edit
 *   options:
 *     query:
 *       type: homepage
 * .
 *
 * @package Drupal\ts_dx\Controller
 */
class ToolbarRedirectController extends ControllerBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * ToolbarRedirectController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entityType manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The requestStack.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Redirect to node edit form selected by the query params.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function nodeEditFormRedirect() {
    return $this->entityEditFormRedirect('node');
  }

  /**
   * Redirect to term edit form selected by the query params.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function termEditFormRedirect() {
    return $this->entityEditFormRedirect('taxonomy_term');
  }

  /**
   * Get a redirect response to the corresponding latest entity form.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function entityEditFormRedirect(string $entity_type_id): RedirectResponse {
    $entities = [];
    try {
      $entities = $this->getEntities($entity_type_id);
    }
    catch (\Exception $e) {
    }
    $entity = end($entities);
    if ($entity) {
      return $this->redirect('entity.' . $entity_type_id . '.edit_form', [$entity_type_id => $entity->id()]);
    }

    return $this->redirect('system.404');
  }

  /**
   * Get the list of entities.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntities(string $entity_type_id): array {
    $query = $this->currentRequest->query->all();
    $query = array_combine(array_map(function ($key) {
      return str_replace(':', '.', $key);
    }, array_keys($query)), $query);

    foreach ($query as $queryElement) {
      if (is_array($queryElement)) {
        return $this->getEntitiesByEntityQuery($entity_type_id, $query);
      }
    }

    return $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->loadByProperties($query);
  }

  /**
   * Get a list of entities according to query parameters.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $query_elements
   *   The query conditions.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The matching entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntitiesByEntityQuery(string $entity_type_id, array $query_elements): array {
    $query = $this->entityTypeManager->getStorage($entity_type_id)
      ->getQuery()
      ->accessCheck(TRUE);
    foreach ($query_elements as $field => $query_element) {
      if (is_array($query_element)) {
        $query->condition($field, $query_element['value'], $query_element['type']);
      }
      else {
        $query->condition($field, $query_element);
      }
    }

    return $this->entityTypeManager->getStorage($entity_type_id)
      ->loadMultiple($query->execute());
  }

}
