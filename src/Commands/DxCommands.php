<?php

namespace Drupal\ts_dx\Commands;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DxCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Separator.
   *
   * @const string
   */
  const SEPARATOR = ',';

  /**
   * NB_ITEMS.
   *
   * @const int
   */
  const NB_ITEMS = 50;

  /**
   * EntityType Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cache entity Types manager.
   *
   * @var array
   */
  protected $storages = [];

  /**
   * Cache definitions.
   *
   * @var array
   */
  protected $definitions = [];

  /**
   * DxCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity Type Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Delete entities by id.
   *
   * @param string $entity_type_id
   *   Entity Type id.
   * @param string $id
   *   Ids to delete (ex: 1,12,1000).
   *
   * @option option-name
   *   Description.
   * @usage x:delete-entity-by-id node 1,2,34
   *   Usage description.
   *
   * @command dx:delete-entity-by-id
   * @aliases dx:dei
   */
  public function deleteEntitiesByIds($entity_type_id = NULL, $id = NULL): void {
    $entity_type_id = $entity_type_id ?: $this->io()->ask(
      'The entity type id ? ',
      'node',
      function ($value) {
        $this->getStorage($value);

        return $value;
      }
    );

    $id = $id ?: $this->io()->ask(
      'The entity id ? ',
      '',
      function ($value) use ($entity_type_id) {
        if (strpos(static::SEPARATOR, $value) === FALSE) {
          if (is_null($this->getStorage($entity_type_id)->load($value))) {
            throw new \Exception('id does not exists');
          }
        }

        return $value;
      }
    );
    $this->deleteAllEntities($entity_type_id, explode(static::SEPARATOR, $id));
  }

  /**
   * Delete entities by id.
   *
   * @param string $entity_type_id
   *   Entity Type id.
   * @param string $bundles
   *   Bundles to delete.
   *
   * @usage dx:delete-entity-by-bundle node article,posts
   *   Usage description
   *
   * @command dx:delete-entity-by-bundles
   * @aliases dx:deb
   */
  public function deleteEntitiesByBundle($entity_type_id, $bundles): void {
    // Get Ids :
    if ($storage = $this->getStorage($entity_type_id)) {
      $type = $this->getDefinition($entity_type_id);

      $entityQuery = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition($type->getKey('bundle'), explode(static::SEPARATOR, $bundles), 'IN');
      $entityIds = $entityQuery->execute();

      $this->deleteAllEntities($entity_type_id, $entityIds);
    }
    else {
      $this->noStorage($entity_type_id);
    }
  }

  /**
   * Delete all entities by type.
   *
   * @param string $entity_type_id
   *   Entity Type id.
   *
   * @usage dx:delete-entity-by-type node
   *   Usage description
   *
   * @command dx:delete-entity-by-type
   * @aliases dx:det
   */
  public function deleteEntitiesByType($entity_type_id): void {
    // Get Ids :
    if ($storage = $this->getStorage($entity_type_id)) {
      $entityIds = $storage->getQuery()
        ->accessCheck(FALSE)
        ->execute();

      $this->deleteAllEntities($entity_type_id, $entityIds);
    }
    else {
      $this->noStorage($entity_type_id);
    }
  }

  /**
   * Delete entities by packages.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $entities_ids
   *   THe entity ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function deleteAllEntities(string $entity_type_id, array $entities_ids): void {
    $entityType = $this->getStorage($entity_type_id);
    $total = count($entities_ids);
    if ($entityType) {
      // Loop by packages.
      $count = 0;
      $errors = 0;
      for ($i = 0; $i < $total; $i += static::NB_ITEMS) {
        $chunk = array_slice($entities_ids, $i, $i + static::NB_ITEMS);
        $entities = $entityType->loadMultiple($chunk);
        // Add entities not found.
        $errors += count($chunk) - count($entities);

        foreach ($entities as $entity) {
          try {
            $entity->delete();
            $count++;
          }
          catch (\Exception $e) {
            $errors++;
          }
        }

        // Clean memory.
        $this->logger->info($this->t('@count/@total (@errors errors) deleted', [
          '@count' => $count,
          '@total' => $total,
          '@errors' => $errors,
        ]));
        $this->cleanMemory();
      }
    }
    else {
      $this->noStorage($entity_type_id);
    }
  }

  /**
   * Clean memory.
   */
  protected function cleanMemory() {
  }

  /**
   * Return the storage.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getStorage(string $entity_type_id): EntityStorageInterface {
    if (!array_key_exists($entity_type_id, $this->storages)) {
      $this->storages[$entity_type_id] = $this->entityTypeManager->getStorage($entity_type_id);
    }

    return $this->storages[$entity_type_id];
  }

  /**
   * Return the storage.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDefinition(string $entity_type_id): EntityTypeInterface {
    if (!array_key_exists($entity_type_id, $this->definitions)) {
      $this->definitions[$entity_type_id] = $this->entityTypeManager->getDefinition($entity_type_id);
    }

    return $this->definitions[$entity_type_id];
  }

  /**
   * Log no storage.
   *
   * @param string $entity_type_id
   *   The entity type id.
   */
  protected function noStorage(string $entity_type_id): void {
    $this->logger->error($this->t('No entity type @type', [
      '@type' => $entity_type_id,
    ]));
  }

}
