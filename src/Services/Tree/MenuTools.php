<?php

namespace Drupal\ts_dx\Services\Tree;

use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Provides menu tools.
 */
class MenuTools {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'ts_dx.menu_tools';

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   */
  public static function instance(): static {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected MenuLinkTreeInterface $menuLinkTree;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   The menu link tree service.
   */
  public function __construct(MenuLinkTreeInterface $menu_link_tree) {
    $this->menuLinkTree = $menu_link_tree;
  }

  /**
   * Get the menu render array.
   *
   * @param string $menu_id
   *   The menu ID.
   * @param array $manipulators
   *   The manipulators list.
   *
   * @return array
   *   The menu build array.
   */
  public function getMenuBuildArray(string $menu_id, array $manipulators = []): array {
    $tree = $this->getMenuLinkTree($menu_id, $manipulators);

    return $this->menuLinkTree->build($tree);
  }

  /**
   * Get the menu tree.
   *
   * @param string $menu_id
   *   The menu ID.
   * @param array $manipulators
   *   The manipulators list.
   * @param string|int|null $root_id
   *   Parent plugin id.
   * @param string|int|null $max_depth
   *   Max depth.
   *
   * @return array
   *   The menu tree render array.
   */
  public function getMenuLinkTree(string $menu_id = 'main', array $manipulators = NULL, $root_id = NULL, $max_depth = NULL): array {

    // Default manipulators.
    $manipulators = $manipulators ?: [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];

    $parameters = new MenuTreeParameters();
    $parameters->excludeRoot()->onlyEnabledLinks();
    $parameters->setRoot($root_id);
    $parameters->setMaxDepth($max_depth);
    $tree = $this->menuLinkTree->load($menu_id, $parameters);

    if (empty($tree)) {
      return [];
    }

    return $this->menuLinkTree->transform($tree, $manipulators);
  }

}
