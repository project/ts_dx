<?php

namespace Drupal\ts_dx\Services\Theme;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\ts_dx\Services\Theme\Base\AbstractTwigExtension;
use Twig\TwigFunction;

/**
 * Several twig tools.
 *
 * @package Drupal\ts_dx\Services\Theme
 */
class TwigExtension extends AbstractTwigExtension {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'ts_dx.twig_extension';

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   */
  public static function instance() {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * Prefix extensions.
   *
   * @const string
   */
  const PREFIX = 'ts_';

  /**
   * Theme Manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Extension Path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * TwigExtension constructor.
   *
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(
    ThemeManagerInterface $theme_manager,
    ExtensionPathResolver $extension_path_resolver
  ) {
    $this->themeManager = $theme_manager;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrefix(): string {
    return static::PREFIX;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllFunctions(): array {
    return [
      new TwigFunction('module_path', [
        $this,
        'getCurrentModulePath',
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllFilters(): array {
    return [];
  }

  /**
   * Get the full path from current (or given) theme name.
   *
   * @param string $path
   *   The relative path.
   * @param string $theme
   *   The theme, if not given, curren theme.
   *
   * @return string
   *   The complete path.
   */
  public function getCurrentModulePath($path, $theme = NULL) {
    if ($theme) {
      $asset_dir = base_path() . $this->extensionPathResolver->getPath('theme', $theme);
    }
    else {
      $asset_dir = $this->themeManager->getActiveTheme()->getPath();
    }

    return '/' . $asset_dir . '/' . $path;
  }

}
