<?php

namespace Drupal\ts_dx\Services\Theme;

use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteObjectInterface;

/**
 * Provides tools for themes.
 *
 * @package Drupal\ts_dx\Services\Theme
 */
class ThemeTools {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'ts_dx.theme_tools';

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   */
  public static function instance(): static {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * Admin Context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected AdminContext $adminContext;

  /**
   * Current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * ThemeTools constructor.
   *
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   */
  public function __construct(
    AdminContext $admin_context,
    CurrentRouteMatch $current_route_match,
  ) {
    $this->adminContext = $admin_context;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * Return true if past route object is admin context.
   *
   * @param \Drupal\Core\Routing\RouteObjectInterface|null $route
   *   The route to check (if null, then check current route).
   *
   * @return bool
   *   True if is admin context.
   */
  public function isAdminContext(RouteObjectInterface $route = NULL) {
    $route = $route ?: $this->currentRouteMatch->getRouteObject();
    return $this->adminContext->isAdminRoute($route);
  }

  /**
   * Sort suggestions by view mode..
   *
   * By default, Drupal use suggestion in this order :
   * - node-type-view-mode
   * - node-type
   * - node-view-mode
   * - node.
   *
   * here we define an more usefull order of suggestions, taking view mode in priority:
   * - node-type-view-mode
   * - node-view-mode
   * - node-type
   * - node.
   *
   * @param array $suggestions
   *   The suggestions.
   * @param array $variables
   *   The variables.
   */
  public function sortSuggestions(array &$suggestions, array $variables) {
    if (isset($variables['elements']['#view_mode'])) {
      $viewMode = $variables['elements']['#view_mode'];

      // Split suggestions in two separated list (view mode suggestion and others).
      $newSuggestions = [];
      foreach ($suggestions as $suggestion) {
        if (strpos($suggestion, $viewMode) !== FALSE) {
          $newSuggestions[0][] = $suggestion;
        }
        else {
          $newSuggestions[1][] = $suggestion;
        }
      }

      // Concat the two list (view_mode and others), so view mode are priorised.
      if (array_key_exists(0, $newSuggestions) && array_key_exists(1, $newSuggestions)) {
        $newSuggestions = array_merge($newSuggestions[1], $newSuggestions[0]);
      }
      // If there is only view mode suggestions, we only use this one.
      elseif (array_key_exists(0, $newSuggestions)) {
        $newSuggestions = $newSuggestions[0];
      }
      // If there is only suggestions without view mode, we only use this one.
      elseif (array_key_exists(1, $newSuggestions)) {
        $newSuggestions = $newSuggestions[1];
      }

      // Reapply indexes.
      foreach ($newSuggestions as $key => $suggestion) {
        $suggestions[$key] = $suggestion;
      }
    }
  }

}
