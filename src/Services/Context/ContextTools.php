<?php

namespace Drupal\ts_dx\Services\Context;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Provides tools for context.
 *
 * @package Drupal\ts_dx\Services\Context
 */
class ContextTools {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'ts_dx.context_tools';

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   */
  public static function instance() {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * Current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * Entity Repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * Current context entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|bool|null
   */
  protected $currentContextEntity = FALSE;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(
    CurrentRouteMatch $current_route_match,
    EntityRepositoryInterface $entity_repository,
  ) {
    $this->currentRouteMatch = $current_route_match;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Return the entity of the current page.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The current context entity.
   */
  public function getCurrentContextEntity(): ?EntityInterface {
    if ($this->currentContextEntity === FALSE) {
      $routeParameters = $this->currentRouteMatch->getParameters()->all();
      $this->currentContextEntity = $this->getMainEntityFromRouteParameters($routeParameters);
    }

    return $this->currentContextEntity;
  }

  /**
   * Return the main (first) entity from route parameters.
   *
   * @param array $route_parameters
   *   The route parameters.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  public function getMainEntityFromRouteParameters(array $route_parameters): ?EntityInterface {
    foreach ($route_parameters as $entity_type_id => $value) {
      if ($value instanceof EntityInterface) {
        return $value;
      }
      try {
        if ($entity = $this->entityRepository->getActive($entity_type_id, $value)) {
          return $entity;
        }
      }
      catch (\Exception $e) {
        // Mute exception...
      }
    }
    return NULL;
  }

}
